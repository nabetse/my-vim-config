if empty(glob('~/.vim/autoload/plug.vim'))
	  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
	      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
" Initialize plugin system
Plug 'patstockwell/vim-monokai-tasty'
Plug 'itchyny/lightline.vim'
call plug#end()
" End plugin system

" lightline config
set laststatus=2
set noshowmode
let g:lightline = {
  \     'active': {
  \         'left': [['mode', 'paste' ], ['readonly', 'filename', 'modified']],
  \         'right': [['lineinfo'], ['percent'], ['fileformat', 'fileencoding']]
  \     }
  \ }

" color scheme monokai-tasty config
let g:vim_monokai_tasty_italic = 1
colorscheme vim-monokai-tasty

""" line numbers
set number
set relativenumber
" Enables cursor line position tracking:
set cursorline
" Removes the underline causes by enabling cursorline:
highlight clear CursorLine
" Sets the line numbering to red background:
highlight CursorLineNR ctermbg=red    
"
" " These settings highlight a vertical cursor column:
" "set cursorcolumn
" "highlight CursorColumn ctermfg=White ctermbg=Yellow cterm=bold guifg=white
" guibg=yellow gui=bold
" "highlight CursorColumn ctermfg=Black ctermbg=Yellow cterm=bold guifg=Black
" guibg=yellow gui=NO

 " indent
  3 set shiftwidth=2
  2 set autoindent
  1 set smartindent
54  set tabstop=2
