# My Vim Config
Personal git repo for my Vim configuration.

## Instalation

Just Clone this git repo and copy .vimrc file.
This will install Plug manager.

## Notes

Pluggin auto install included.

The first time vim is reloaded it will complain that pluggins are not installed.
Just let it install them and reopen vim.
Vim shouldn't complain any more.
